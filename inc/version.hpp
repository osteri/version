#pragma once

#include <semver.hpp>

namespace git {
extern semver::version get_version();
}  // namespace git
