add_library(${PROJECT_NAME}-header INTERFACE)
target_include_directories(${PROJECT_NAME}-header SYSTEM INTERFACE ${CMAKE_CURRENT_SOURCE_DIR} semver)
target_compile_features(${PROJECT_NAME}-header INTERFACE cxx_std_17)