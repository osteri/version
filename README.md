# C++ version library

Built-in compile-time semantic versioning from `git` tags. Requires C++17.

Very thin wrapper around [semver](https://github.com/Neargye/semver) using compile-time version numbering from git tags.

The library accepts following kind of tags (or versions):
- 1.2.3
- 1.2.3-rc.0

Thanks to `constexpr` and C++17, library fails to compile without 
[semver compliant](https://semver.org/#semantic-versioning-specification-semver) tags. This is a great thing, no 
runtime surprises!

Parsing of `git` tags happens in [cmake/version.cmake](https://gitlab.com/osteri/version/blob/master/cmake/version.cmake)

## Basic usage

```cpp
#include <iostream>
#include <version.hpp>

int main() {
  std::cout << "software version: " << git::get_version() << '\n';
  try {
    std::cout << "0.0.0-rc.0 is smaller version than " << git::get_version()
              << "? " << std::boolalpha
              << (git::get_version() > semver::version{"0.0.0-rc.0"}) << '\n';
  } catch (const std::runtime_error& e) {
    std::cerr << "comparison failed: " << e.what() << '\n';
  }
}
```

See more usage examples in [example](https://gitlab.com/osteri/cpp-bootstrap/blob/master/example/) directory.

## Adding library as a git submodule in CMake

Clone the library:
```
git submodule add https://gitlab.com/osteri/version
git submodule update --init --recursive
```
Include this library in your main `CMakeLists.txt` like this:
```
add_subdirectory(version)
target_link_libraries(main version-lib)
```

## Build all

```
cmake -S . -B build -DBUILD_EXAMPLES=ON -DBUILD_TESTS=ON && (cd build && make -j)
```

## More info

Warning: comparing different kind of version may throw. For example: release candidates (1.2.3-rc.0) cannot be compared with releases (1.2.3). More info on [semver](https://github.com/Neargye/semver).
