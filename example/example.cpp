#include <iostream>
#include <version.hpp>

int main() {
  std::cout << "software version: " << git::get_version() << '\n';
  try {
    std::cout << "0.0.0-rc.0 is smaller version than " << git::get_version()
              << "? " << std::boolalpha
              << (git::get_version() > semver::version{"0.0.0-rc.0"}) << '\n';
  } catch (const std::runtime_error& e) {
    std::cerr << "comparison failed: " << e.what() << '\n';
  }
}