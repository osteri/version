execute_process(
  COMMAND git describe --tags
  OUTPUT_VARIABLE GIT_TAG ERROR_QUIET)

string(STRIP "${GIT_TAG}" GIT_TAG)

if(NOT WIN32)
  string(ASCII 27 Esc)
  set(ColorReset "${Esc}[m")
  set(ColorBold  "${Esc}[1m")
  set(Red         "${Esc}[31m")
  set(Green       "${Esc}[32m")
  set(Yellow      "${Esc}[33m")
  set(White       "${Esc}[37m")
  set(BoldWhite   "${Esc}[1;37m")
  # combined
  set(TagColor    "${ColorBold}${Esc}[7;49;93m")
  set(COLORED_GIT_TAG "${TagColor}${GIT_TAG}${ColorReset}")
endif()

# Whitelisted tags
# For example:
# - 1.2.3
# - 1.2.3-rc.0
set(RELEASE_REGEX "^([0-9]+)\\.([0-9]+)\\.([0-9]+)$")
set(RC_REGEX "^([0-9]+)\\.([0-9]+)\\.([0-9]+)-rc\\.([0-9]+)$")

# Prevent compilation if git tag does not match semantic versioning. By
# default it should be always true, otherwise semver may throw at runtime.
set(STRICT_SEMVER FALSE)

if(GIT_TAG MATCHES ${RELEASE_REGEX})
  message(STATUS "Found suitable release tag: ${COLORED_GIT_TAG}")
elseif(GIT_TAG MATCHES ${RC_REGEX})
  message(STATUS "Found suitable release candidate tag: ${COLORED_GIT_TAG}")
elseif(NOT STRICT_SEMVER)
  message(WARNING "Project has commit in its version string: ${COLORED_GIT_TAG}")
else()
  message(FATAL_ERROR "Project has commit in its version string: ${COLORED_GIT_TAG}")
endif()

# Generate version.cpp
set(VERSION "#include <version.hpp>

namespace git {
constexpr semver::version __internal_git_semver{\"${GIT_TAG}\"};
semver::version get_version() { return __internal_git_semver; }
} // namespace git
")

if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/version.cpp)
  file(READ ${CMAKE_CURRENT_SOURCE_DIR}/version.cpp VERSION_)
else()
  set(VERSION_ "")
endif()

if(NOT "${VERSION}" STREQUAL "${VERSION_}")
  message(STATUS "Generated ${CMAKE_CURRENT_SOURCE_DIR}/version.cpp")
  file(WRITE ${CMAKE_CURRENT_SOURCE_DIR}/version.cpp "${VERSION}")
endif()
